const burgerBtn = document.getElementById("burger-button");
const menu = document.querySelector(".header-menu");
let currBtn;
burgerBtn.addEventListener("click", () => {
    currBtn = document.querySelector(".burger-active");
    if (currBtn.classList.contains("fa-bars")) {
        currBtn.nextElementSibling.classList.add("burger-active");
        menu.classList.add("burger-active");
        currBtn.classList.remove("burger-active");
    } else {
        currBtn.previousElementSibling.classList.add("burger-active");
        menu.classList.remove("burger-active");
        currBtn.classList.remove("burger-active");
    }
});
