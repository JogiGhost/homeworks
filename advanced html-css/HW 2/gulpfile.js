const gulp = require("gulp");
const browserSync = require("browser-sync").create();
const concat = require("gulp-concat");

gulp.task("compile-css", () => {
    const sass = require("gulp-sass")(require("sass"));
    return gulp
        .src("./src/scss/*.scss")
        .pipe(sass().on("error", sass.logError))
        .pipe(gulp.dest("./src/css"));
});
gulp.task("clean", () => {
    const clean = require("gulp-clean");
    return gulp.src("./dist/*", { read: false }).pipe(clean());
});
gulp.task("autoprefixer", () => {
    const autoprefixer = require("autoprefixer");
    const sourcemaps = require("gulp-sourcemaps");
    const postcss = require("gulp-postcss");

    return gulp
        .src("./src/css/*.css")
        .pipe(sourcemaps.init())
        .pipe(postcss([autoprefixer()]))
        .pipe(sourcemaps.write("."))
        .pipe(gulp.dest("./src/css"));
});
gulp.task("minify-css", () => {
    const cleanCSS = require("gulp-clean-css");
    return gulp
        .src("./src/css/*.css")
        .pipe(concat("styles.min.css"))
        .pipe(cleanCSS())
        .pipe(gulp.dest("./src/css/minified"))
        .pipe(gulp.dest("./dist"))
        .pipe(browserSync.stream());
});
gulp.task("minify-js", () => {
    const uglify = require("gulp-uglify");
    return gulp
        .src("./src/js/*.js")
        .pipe(concat("scripts.min.js"))
        .pipe(uglify())
        .pipe(gulp.dest("./src/js/minified"))
        .pipe(gulp.dest("./dist"));
});
gulp.task("js-watch", gulp.series("minify-js"), (done) => {
    browserSync.reload();
    done();
});
gulp.task("minify-image", () => {
    const compress_images = require("compress-images");
    let INPUT_path, OUTPUT_path;
    INPUT_path = "src/img/*.{jpg,JPG,jpeg,JPEG,png,svg,gif}";
    OUTPUT_path = "dist/img/";
    compress_images(
        INPUT_path,
        OUTPUT_path,
        { compress_force: false, statistic: true, autoupdate: true },
        false,
        { jpg: { engine: "mozjpeg", command: ["-quality", "60"] } },
        { png: { engine: "pngquant", command: ["--quality=20-50", "-o"] } },
        { svg: { engine: "svgo", command: "--multipass" } },
        {
            gif: {
                engine: "gifsicle",
                command: ["--colors", "64", "--use-col=web"],
            },
        },
        function (error, completed, statistic) {
            console.log("-------------");
            console.log(error);
            console.log(completed);
            console.log(statistic);
            console.log("-------------");
        }
    );
});
gulp.task(
    "build",
    gulp.series(
        "clean",
        gulp.parallel(
            "minify-image",
            "minify-js",
            gulp.series("compile-css", "autoprefixer", "minify-css")
        )
    )
);

gulp.task("dev", () => {
    browserSync.init({
        server: "./",
    });

    gulp.watch(
        "src/scss/*.scss",
        gulp.series("compile-css", "autoprefixer", "minify-css")
    );
    gulp.watch("src/js/*.js", gulp.series("js-watch"));
    gulp.watch("./*.html").on("change", browserSync.reload);
});
