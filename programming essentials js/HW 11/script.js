let oldBtn = null;
document.addEventListener("keyup", (e) => {
    if (oldBtn) {
        oldBtn.style = "";
    }
    const btn = document.querySelector(
        `button[data-name="${e.key.toLocaleLowerCase()}"]`
    );
    if (btn) {
        btn.style.background = "blue";
        oldBtn = btn;
    }
});
