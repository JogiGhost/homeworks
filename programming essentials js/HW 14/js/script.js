$(document).on("click", 'a[href^="#"]', function (e) {
    $("html").animate(
        {
            scrollTop: $($.attr(this, "href")).offset().top,
        },
        600
    );
    e.preventDefault();
});
$(document).on("scroll", function (e) {
    if (document.documentElement.scrollTop > window.innerHeight) {
        $("#scrollTop").css("display", "block");
    } else {
        $("#scrollTop").css("display", "none");
    }
    e.preventDefault();
});
$("#scrollTop").click(function (e) {
    $("html").animate({ scrollTop: 0 }, 600);
    e.preventDefault();
});
$("#show-content").on("click", function (e) {
    $(".hot-news-wrap").slideToggle("slow");
    e.preventDefault();
});
