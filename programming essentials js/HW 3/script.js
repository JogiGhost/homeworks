
function counter(firstNum, secondNum, operation) {
    // знаю что здесь нету резона тернарник применять, мне просто было интересно с ним попрактиковаться
    // я бы применил здесь switch-case, если не хотел сделать через тернарный оператор
    let result = (operation === "+") ? Number(firstNum) + Number(secondNum) :
        (operation === "-") ? Number(firstNum) - Number(secondNum) :
            (operation === "/") ? Number(firstNum) / Number(secondNum) :
                Number(firstNum) * Number(secondNum);
    return result;
}

let firstNumber, secondNumber, operation;

do {
    firstNumber = prompt("Enter firstNumber");
    operation = prompt("Enter operation");
    secondNumber = prompt("Enter secondNumber");
} while ((firstNumber && secondNumber && operation) === ""
|| (firstNumber && secondNumber && operation) === "null"
|| (operation !== "+" && operation !== "-" && operation !== "*" && operation !== "/")
    || isNaN(Number(firstNumber) && Number(secondNumber)));

console.log(counter(firstNumber, secondNumber, operation));