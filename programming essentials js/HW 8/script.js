const priceInput = document.getElementById("price");

priceInput.addEventListener("focus", (e) => {
    priceInput.classList.remove("unfocus");
    priceInput.classList.remove("incorrect");
    priceInput.classList.add("focus");
});

priceInput.addEventListener("blur", (e) => {
    const getP = document.querySelector("p");

    if (
        priceInput.value.trim() === "" ||
        isNaN(priceInput.value) ||
        Number(priceInput.value) < 0
    ) {
        priceInput.classList.remove("focus");
        priceInput.classList.add("incorrect");

        if (getP) {
            return;
        }
        priceInput.insertAdjacentHTML(
            "afterend",
            `<p>Please enter correct price</p>`
        );
        return;
    }

    priceInput.classList.remove("incorrect");
    priceInput.classList.remove("focus");
    priceInput.classList.add("unfocus");

    getP.remove();
    document.body.insertAdjacentHTML(
        "afterbegin",
        `<span>  Текущая цена: ${priceInput.value} <button>X</button></span>`
    );
});

document.body.addEventListener("click", (e) => {
    if (e.target.innerText === "X") {
        e.target.previousSibling.remove();
        e.target.remove();

        priceInput.value = "";
    }
});
