const createNewUser = () => {
    let firstName, secondName, date;
    firstName = prompt("Enter first name");
    secondName = prompt("Enter second name");
    date = new Date(prompt("Enter date (dd.mm.yyyy)"));
    const newUser = {
        firstName: firstName,
        lastName: secondName,
        birthday: date,
        set setFirstName(firstName) {
            Object.defineProperties(newUser, { firstName: { writable: true } });
            this.firstName = firstName;
        },
        set setLastName(secondName) {
            Object.defineProperties(newUser, { lastName: { writable: true } });
            this.lastName = secondName;
        },
        getLogin() {
            return (this.firstName[0] + this.lastName).toLowerCase();
        },
        getAge() {
            const difference = Date.now() - this.birthday.getTime();
            const ageDate = new Date(difference);
            return Math.abs(ageDate.getFullYear() - 1970);
        },
        getPassword() {
            return (
                this.firstName[0].toUpperCase() +
                (this.lastName + this.birthday.getFullYear()).toLowerCase()
            );
        },
    };
    Object.defineProperties(newUser, {
        firstName: { writable: false },
        lastName: { writable: false },
    });
    return newUser;
};

let newUser = createNewUser();
console.log(newUser);
console.log(newUser.getAge(), newUser.getPassword());
