// я ввод данных вынес в отдельную функцию чтобы код был более читаемый, не бей, пожалуйста

const inputData = () => {
    let firstName, secondName;
    do {
        firstName = prompt("Enter first name");
        secondName = prompt("Enter second name");
    } while (
        (firstName && secondName) === "" ||
        (firstName && secondName) == null ||
        !isNaN(firstName && secondName)
    );
    return createNewUser(firstName, secondName);
};

const createNewUser = (firstName, secondName) => {
    const newUser = {
        firstName: firstName,
        lastName: secondName,
        set setFirstName(firstName) {
            Object.defineProperties(newUser, { firstName: { writable: true } });
            this.firstName = firstName;
        },
        set setLastName(secondName) {
            Object.defineProperties(newUser, { lastName: { writable: true } });
            this.lastName = secondName;
        },
        getLogin() {
            return (this.firstName[0] + this.lastName).toLowerCase();
        },
    };
    Object.defineProperties(newUser, {
        firstName: { writable: false },
        lastName: { writable: false },
    });
    newUser.setLastName = "Anton"; //advanced task
    return newUser.getLogin();
};

console.log(inputData());
