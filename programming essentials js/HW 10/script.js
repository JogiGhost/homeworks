const form = document.querySelector(".password-form");

form.addEventListener("click", (e) => {
    if (
        e.target.classList.contains("active") &&
        e.target.classList.contains("fa-eye")
    ) {
        const newEye = e.target.nextElementSibling;
        e.target.classList.remove("active");
        newEye.classList.add("active");

        const input = e.target.previousElementSibling;
        input.type = "text";
    } else if (
        e.target.classList.contains("active") &&
        e.target.classList.contains("fa-eye-slash")
    ) {
        const newEye = e.target.previousElementSibling;
        e.target.classList.remove("active");
        newEye.classList.add("active");

        const input = e.target.previousElementSibling.previousElementSibling;
        input.type = "password";
    } else if (e.target.classList.contains("btn")) {
        const enterPassword =
            e.target.previousElementSibling.previousElementSibling
                .firstElementChild;
        const submitPassword =
            e.target.previousElementSibling.firstElementChild;

        if (enterPassword.value && submitPassword.value) {
            const errorMessage = document.querySelector(".invalid-password");

            if (enterPassword.value === submitPassword.value) {
                enterPassword.value = submitPassword.value = "";
                if (errorMessage) {
                    errorMessage.remove();
                }
                alert("You are welcome");
            } else {
                if (errorMessage) {
                    return;
                }
                submitPassword.insertAdjacentHTML(
                    "afterend",
                    `<p class="invalid-password">Нужно ввести одинаковые значения</p>`
                );
            }
        }
    }
    e.preventDefault();
});
