const addList = (array, parent = document.body) => {
    const newArray = array.map((element) => `<li>${element}</li>`);
    if (parent.toString() === "[object HTMLBodyElement]") {
        parent.insertAdjacentHTML("beforeend", `<ul>${newArray.join("")}</ul>`);
    } else {
        const newParent = document.createElement(parent);
        document.body.insertAdjacentElement("beforeend", newParent);
        newParent.insertAdjacentHTML(
            "beforeend",
            `<ul>${newArray.join("")}</ul>`
        );
    }
};

addList(["hello", "world", "Kiev", "Kharkiv", "Odessa", "Lviv"]);
addList(["1", "2", "3", "sea", "user", 23], "div");
