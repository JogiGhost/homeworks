const imgWrap = document.querySelector(".images-wrapper");
let flag = false;
let oldImg = null;
let timeout = null;

const showNextImg = () => {
    if (flag) {
        oldImg.classList.remove("active");
        imgWrap.firstElementChild.classList.add("active");
        flag = false;
        timeout = setTimeout(showNextImg, 3000);
    } else {
        oldImg = document.querySelector(".active");
        oldImg.classList.remove("active");
        oldImg = oldImg.nextElementSibling;
        oldImg.classList.add("active");
        if (oldImg.dataset.end) {
            flag = true;
        }
        timeout = setTimeout(showNextImg, 3000);
    }
};
timeout = setTimeout(showNextImg, 3000);

imgWrap.insertAdjacentHTML(
    "afterend",
    `<button type="button" id="stop">Прекратить</button> <button type="button" id="resume" disabled>Возобновить показ</button>`
);

const stopSlider = document.getElementById("stop");
const resumeSlider = document.getElementById("resume");

stopSlider.addEventListener("click", (e) => {
    clearTimeout(timeout);
    resumeSlider.disabled = false;
    stopSlider.disabled = true;
});
resumeSlider.addEventListener("click", (e) => {
    timeout = setTimeout(showNextImg, 3000);
    resumeSlider.disabled = true;
    stopSlider.disabled = false;
});
