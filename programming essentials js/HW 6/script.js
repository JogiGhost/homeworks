const filterBy = (array, type) =>
    array.filter((elem) => {
        if (type === "null" && elem === null) {
            return false;
        } else if (typeof elem !== type) {
            return true;
        }
    });

console.log(
    filterBy(["hello", "world", "23", 23, null, undefined, true], "null")
);
