let ageOfUser, nameOfUser;

do {
    ageOfUser = +prompt("Enter your age");
    nameOfUser = prompt("Enter your name");
} while (isNaN(ageOfUser) || ageOfUser === 0 || !isNaN(Number(nameOfUser)));

if (ageOfUser > 22) {
    alert(`Welcome, ${nameOfUser}`);
}
else if (ageOfUser >= 18 && ageOfUser <= 22) {
    const choice = confirm("Are you sure you want to continue?");
    if (choice) {
        alert(`Welcome, ${nameOfUser}`);
    }
    else {
        alert("You are not allowed to visit this website");
    }
}
else {
    alert("You are not allowed to visit this website");
}

// 1. var, let, const - это зарезервированные слова для объявления переменных.

//     Var - это устаревшее ключевое слово для объявления переменных, который использовался в версиях ES5 и ниже.

//     Let - это ключевое слово для объявления переменных, значения и тип которых в будущем можно изменять, впервые появился в ES6
//     и посвеместно используется вместо устаревшего var

//     Const - это ключевое слово для объявления констант, главное различие с let в том, что значения и тип примитивных констант менять
//     нельзя, хотя, например, значения, которые находятся в объектах могут быть подвержены изменению

// 2. Var - это устаревший способ объявления переменных, главным ньюансом которого я считаю то, что такие переменные ВСЕГДА находятся
//    в глобальной области видимости, это означает что они могут быть случайно изменены из любой части кода.
//    Так же var всегда обрабатываются в самом начале функции независимо от того где происходит присвоение им значений