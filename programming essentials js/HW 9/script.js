const tab = document.querySelector(".tabs");
let li = null;
tab.addEventListener("click", (e) => {
    if (li) {
        li.style.display = "none";
    }
    if (e.target.classList.contains("tabs")) {
        return;
    }
    const oldTab = document.querySelector(".active");
    if (oldTab) {
        oldTab.classList.remove("active");
    }
    e.target.classList.add("active");
    li = document.querySelector(`li[data-name = "${e.target.innerText}"]`);
    li.style.display = "block";
});
