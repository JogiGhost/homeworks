let number;

do {
    number = prompt("Enter number");
} while (Number(number) % 1 !== 0 || number === "");

for (let i = 0; i <= number; i++) {
    if (number < 5) {
        alert("Sorry, no numbers");
        break;
    }
    else if (i % 5 == 0) {
        console.log(i);
    }
}

// циклы нужны для того, чтобы выполнять одинаковые действия до тех пор, пока не будет достигнута необходимая цель
// циклы могут решать абслютно разные задачи: от валидации данных и до сортировки массивов