let theme = window.localStorage.getItem("theme");
const stylesheet = document.getElementById("style-link");
const themeBtn = document.getElementById("theme-btn");

if (!theme) {
    window.localStorage.setItem("theme", "default");
    stylesheet.href = "./css/style.css";
} else if (theme === "default") {
    stylesheet.href = "./css/style.css";
} else if (theme === "dark") {
    stylesheet.href = "./css/style-dark.css";
}
themeBtn.addEventListener("click", (e) => {
    theme = window.localStorage.getItem("theme");
    themeBtn.value = theme;
    if (themeBtn.value === "default") {
        stylesheet.href = "./css/style-dark.css";
        window.localStorage.setItem("theme", "dark");
    } else {
        stylesheet.href = "./css/style.css";
        window.localStorage.setItem("theme", "default");
    }
});
