async function getAllUsers() {
    return await (
        await fetch("https://ajax.test-danit.com/api/json/users")
    ).json();
}
async function getAllPosts() {
    return await (
        await fetch("https://ajax.test-danit.com/api/json/posts")
    ).json();
}
function selectUser(usersArr, userId) {
    const user = usersArr[userId - 1];
    return user;
}
class Card {
    constructor({ name, email }, { id, userId, title, body }) {
        this.name = name;
        this.email = email;
        this.postId = id;
        this.userPostedId = userId;
        this.title = title;
        this.description = body;

        this.userPost = document.createElement("div");
        this.userPost.className = ["card-div"];
        this.deleteBtn = document.createElement("button");
        this.deleteBtn.textContent = "Delete";
    }
    render() {
        this.userPost.insertAdjacentHTML(
            "afterbegin",
            `<h2>${this.name}</h2> 
            <p>${this.email}</p>
            <h3>${this.title}</h3>
            <p>${this.description}</p>`
        );
        this.userPost.insertAdjacentElement("beforeend", this.deleteBtn);
        this.deleteBtn.addEventListener("click", this.deleteCard);
        return this.userPost;
    }
    deleteCard = async () => {
        try {
            const deleteResponse = await fetch(
                `https://ajax.test-danit.com/api/json/posts/${this.postId}`,
                {
                    method: "DELETE",
                }
            );
            console.log(deleteResponse);
            this.userPost.remove();
        } catch (err) {
            console.log(err);
        }
    };
}
(async () => {
    const root = document.getElementById("root");
    const users = await getAllUsers();
    const posts = await getAllPosts();
    posts.forEach((post) => {
        const card = new Card(selectUser(users, post.userId), post);
        root.insertAdjacentElement("beforeend", card.render());
    });
})();
