async function showAdress() {
    const clientAddress = await (
        await fetch("https://api.ipify.org/?format=json")
    ).json();
    const address = await (
        await fetch(
            `http://ip-api.com/json/${clientAddress.ip}?fields=status,message,continent,country,region,city,district,query`
        )
    ).json();
    const { continent, country, region, city } = address;
    root.insertAdjacentHTML(
        "beforeend",
        `<p id="user-address">${continent}, ${country}, ${region}, ${city}</p>
    `
    );
}
ipBtn.addEventListener("click", () => {
    const userAddress = document.getElementById("user-address");
    if (userAddress) {
        userAddress.remove();
    }
    showAdress();
});
