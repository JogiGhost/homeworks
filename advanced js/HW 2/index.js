const books = [
    {
        author: "Скотт Бэккер",
        name: "Тьма, что приходит прежде",
        price: 70,
    },
    {
        author: "Скотт Бэккер",
        name: "Воин-пророк",
    },
    {
        name: "Тысячекратная мысль",
        price: 70,
    },
    {
        author: "Скотт Бэккер",
        name: "Нечестивый Консульт",
        price: 70,
    },
    {
        author: "Дарья Донцова",
        name: "Детектив на диете",
        price: 40,
    },
    {
        author: "Дарья Донцова",
        name: "Дед Снегур и Морозочка",
    },
];
const root = document.getElementById("root");
const ul = document.createElement("ul");

books.forEach((e) => {
    const { author, name, price } = e;
    try {
        if (!author) {
            throw new Error("Autor field is empty!");
        } else if (!name) {
            throw new Error("Name field is empty!");
        } else if (!price) {
            throw new Error("Price field is empty!");
        }
    } catch (error) {
        console.log(error);
        return;
    }
    ul.insertAdjacentHTML(
        "beforeend",
        `<li>author: ${author}, name: ${name}, price: ${price}</li>`
    );
});

root.insertAdjacentElement("afterbegin", ul);
