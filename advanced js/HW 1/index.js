class Employee {
    constructor(name, age, salary) {
        this._name = name;
        this._age = age;
        this._salary = salary;
    }
    set name(name) {
        this._name = name;
    }
    get name() {
        return this._name;
    }
    set age(age) {
        this._age = age;
    }
    get age() {
        return this._age;
    }
    set salary(salary) {
        this._salary = salary;
    }
    get salary() {
        return this._salary;
    }
}

class Programmer extends Employee {
    constructor(name, age, salary, lang) {
        super(name, age, salary);
        this._lang = lang;
    }
    get salary() {
        return this._salary * 3;
    }
}

const programmer1 = new Programmer("John", 24, 3000, [
    "English",
    "Ukrainian",
    "Chinese",
]);
const programmer2 = new Programmer("Mike", 20, 1000, ["English", "Russian"]);
const programmer3 = new Programmer("Alex", 18, 500, ["English", "Indian"]);

console.log(programmer1);
console.log(programmer2);
console.log(programmer3);
