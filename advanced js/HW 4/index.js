function fetchingActors(arr, element) {
    Promise.all(arr.characters.map((u) => fetch(u)))
        .then((responses) => Promise.all(responses.map((res) => res.json())))
        .then((data) => {
            const characters = data.map((character) => character.name);
            element.insertAdjacentHTML(
                "beforeend",
                `<br><strong>${characters}</strong>`
            );
        });
}
const ul = document.createElement("ul");
fetch("https://ajax.test-danit.com/api/swapi/films")
    .then((response) => response.json())
    .then((data) => {
        const dataArr = data.map((e) => e);
        dataArr.forEach((e) => {
            const li = document.createElement("li");
            fetchingActors(e, li);
            li.insertAdjacentHTML(
                "beforeend",
                `${e.episodeId} ${e.name} <br> ${e.openingCrawl}<br>`
            );
            ul.insertAdjacentElement("beforeend", li);
        });
        console.log(dataArr);
    });
document.body.insertAdjacentElement("beforeend", ul);
