import { useEffect, useRef } from "react";
import { useDispatch, useSelector } from "react-redux";
import Header from "./components/Header/Header";
import Router from "./Router/Router";
import { getProducts } from "./store/products/actions";
import { getCartList } from "./store/addToCart/actions";
import { getFavouritesList } from "./store/favourites/actions";

function App() {
    const isNotInitialMount = useRef(false);
    const dispatch = useDispatch();
    const favoritesList = useSelector((state) => state.favouritesList);
    const addToCartList = useSelector((state) => state.addToCartList);

    useEffect(() => {
        dispatch(getProducts());
        dispatch(getCartList());
        dispatch(getFavouritesList());
    }, [dispatch]);

    useEffect(() => {
        if (isNotInitialMount.current) {
        }
        isNotInitialMount.current = true;
    }, [favoritesList, addToCartList]);
    return (
        <>
            <Header />
            <Router />
        </>
    );
}

export default App;
