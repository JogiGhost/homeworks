import { useState } from "react";
import Button from "../Button/Button";
import Modal from "../Modal/Modal";
import Product from "../Product/Product";
import { PropTypes } from "prop-types";
import "./style.scss";
import { useDispatch, useSelector } from "react-redux";
import { toggleModal } from "../../store/modal/actions";
import { deleteCart, setAddToCartList } from "../../store/addToCart/actions";
import { setFavouritesList } from "../../store/favourites/actions";

function Main(props) {
    const [data, setData] = useState("");
    const [index, setIndex] = useState(null);
    const { notHideAddToCardBtn, modalTitle } = props;
    const favouritesList = useSelector((state) => state.favouritesList);
    const dispatch = useDispatch();
    const modal = useSelector((state) => state.isOpenModal);

    const handleClick = (info, index = null, toRemove = false) => {
        document.body.style.overflow = "hidden";
        dispatch(toggleModal(true));
        setIndex(index);
        setData({ info: info, toRemove: toRemove });
    };
    const handleCloseClick = (e) => {
        e.stopPropagation();
        if (e.target.className.includes("close-function")) {
            document.body.style.overflow = "";
            dispatch(toggleModal(false));
        }
    };
    const handleAddToFavClick = (data) => {
        dispatch(setFavouritesList(data));
    };
    const handleConfirmClick = () => {
        if (data.toRemove) {
            handleDeleteCart(index);
            return;
        }
        handleAddToCartClick(data.info);
    };
    const handleAddToCartClick = (product) => {
        document.body.style.overflow = "";
        dispatch(setAddToCartList(product));
    };
    const handleDeleteCart = (index) => {
        dispatch(deleteCart(index));
    };
    const modalConfig = {
        header: modalTitle,
        actions: (
            <>
                <Button
                    backgroundColor="green"
                    text="Confirm"
                    className="close-function"
                    onClick={handleConfirmClick}
                />
                <Button
                    backgroundColor="red"
                    text="Cancel"
                    className="close-function"
                />
            </>
        ),
        closeFunction: handleCloseClick,
    };
    return (
        <main className="main">
            {props.products &&
                props.products.map((product, index) => {
                    let svgClassName;
                    if (
                        favouritesList.some(
                            (elem) => product.title === elem.title
                        )
                    ) {
                        svgClassName = "bi bi-star bi-star-cart favourite";
                    } else {
                        svgClassName = "bi bi-star bi-star-cart";
                    }
                    return (
                        <Product
                            key={notHideAddToCardBtn ? product.art : index}
                            index={!notHideAddToCardBtn && index}
                            data={product}
                            handleClick={handleClick}
                            handleCloseClick={handleCloseClick}
                            svgClassName={svgClassName}
                            handleAddToCartClick={handleConfirmClick}
                            favourites={handleAddToFavClick}
                            notHideAddToCardBtn={notHideAddToCardBtn}
                        />
                    );
                })}
            {modal && <Modal data={modalConfig} />}
        </main>
    );
}
Main.defaultProps = {
    modalTitle: null,
    notHideAddToCardBtn: true,
};
Main.propTypes = {
    products: PropTypes.oneOfType([PropTypes.string, PropTypes.array]),
    handleAddToFavClick: PropTypes.oneOfType([
        PropTypes.string,
        PropTypes.func,
    ]),
    handleAddToCartClick: PropTypes.oneOfType([
        PropTypes.string,
        PropTypes.func,
    ]),
};
export default Main;
