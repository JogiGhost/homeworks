import { useSelector } from "react-redux";
import Main from "../../components/Main/Main";
import "./style.scss";

export default function Favourites() {
    const favouritesList = useSelector((state) => state.favouritesList);
    return (
        <>
            <h2 className="favourites-title">Favourites</h2>
            <Main
                modalTitle={"Add to Cart ?"}
                products={favouritesList}
                favoritesList={favouritesList}
            />
        </>
    );
}
