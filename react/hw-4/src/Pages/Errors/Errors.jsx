import "./style.scss";

export default function Error() {
    return <h2 className="error-title">ERROR 404. PAGE NOT FOUND!</h2>;
}
