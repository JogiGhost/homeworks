import { useSelector } from "react-redux";
import Main from "../../components/Main/Main";
import "./style.scss";

export default function Basket() {
    const addToCartList = useSelector((state) => state.addToCartList);
    return (
        <>
            <h2 className="basket-title">Basket</h2>
            <Main
                products={addToCartList}
                notHideAddToCardBtn={false}
                modalTitle="Remove from basket?"
            />
        </>
    );
}
