const addToCartReducer = (state = [], action) => {
    switch (action.type) {
        case "GET_CART_LIST":
            return action.payload;
        case "SET_CART_LIST":
            return [...state, action.payload];
        case "DELETE_CART":
            const newCartList = state.filter((_, i) => i !== action.payload);
            return newCartList;
        default:
            return state;
    }
};
export default addToCartReducer;
