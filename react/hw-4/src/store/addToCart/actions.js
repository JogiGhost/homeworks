export const getCartList = () => (disaptch) => {
    const counters = localStorage.getItem("counters");
    const { addToCartList } = JSON.parse(counters)
        ? JSON.parse(counters)
        : { addToCartList: [] };
    disaptch({ type: "GET_CART_LIST", payload: addToCartList });
};
export const setAddToCartList = (data) => (dispatch) =>
    dispatch({
        type: "SET_CART_LIST",
        payload: data,
    });
export const deleteCart = (data) => (dispatch) =>
    dispatch({
        type: "DELETE_CART",
        payload: data,
    });
