export const toggleModal = (data) => (dispatch) =>
    dispatch({
        type: "TOGGLE_MODAL",
        payload: data,
    });
