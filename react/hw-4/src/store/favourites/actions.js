export const getFavouritesList = () => (dispatch) => {
    const counters = localStorage.getItem("counters");
    const { favoritesList } = JSON.parse(counters)
        ? JSON.parse(counters)
        : { favoritesList: [] };
    dispatch({ type: "GET_FAVOURITES_LIST", payload: favoritesList });
};
export const setFavouritesList = (data) => (dispatch) =>
    dispatch({
        type: "SET_FAVOURITES_LIST",
        payload: data,
    });
