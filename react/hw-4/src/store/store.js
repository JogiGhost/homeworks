import {
    legacy_createStore as createStore,
    applyMiddleware,
    compose,
} from "redux";
import thunk from "redux-thunk";
import mainReducer from "./reducers-index";

const devTools = window.__REDUX_DEVTOOLS_EXTENSION__
    ? window.__REDUX_DEVTOOLS_EXTENSION__()
    : (f) => f;

const initialState = {
    isOpenModal: false,
    products: [],
    favouritesList: [],
    addToCartList: [],
};

const addToLS = (store) => (next) => (action) => {
    next(action);
    if (
        action.type === "SET_CART_LIST" ||
        action.type === "SET_FAVOURITES_LIST" ||
        action.type === "DELETE_CART"
    ) {
        const { favouritesList, addToCartList } = store.getState();
        localStorage.setItem(
            "counters",
            JSON.stringify({
                favoritesList: favouritesList,
                addToCartList: addToCartList,
            })
        );
    }
};

const store = createStore(
    mainReducer,
    initialState,
    compose(applyMiddleware(thunk, addToLS), devTools)
);

export default store;
