import { useSelector } from "react-redux";
import { Route, Routes } from "react-router-dom";
import Main from "../components/Main/Main";
import Basket from "../Pages/Basket/Basket";
import Error from "../Pages/Errors/Errors";
import Favourites from "../Pages/Favourites/Favourites";

export default function Router() {
    const products = useSelector((state) => state.products);
    return (
        <Routes>
            <Route path="/">
                <Route
                    index
                    element={
                        <Main products={products} modalTitle="Add to Cart ?" />
                    }
                ></Route>
                <Route path="basket" element={<Basket />}></Route>
                <Route path="favourites" element={<Favourites />}></Route>
            </Route>
            <Route path="*" element={<Error />}></Route>
        </Routes>
    );
}
