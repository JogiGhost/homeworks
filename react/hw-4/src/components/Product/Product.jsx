import Button from "../Button/Button";
import PropTypes from "prop-types";
import "./style.scss";

function Product(props) {
    const favourites = () => {
        props.favourites(props.data);
    };
    const handleClick = (e) => {
        if (e.target.dataset.toRemove) {
            props.handleClick(props.data, props.index, true);
            return;
        }
        props.handleClick(props.data, props.index);
    };
    const { title, price, img } = props.data;
    return (
        <div className="product">
            {props.notHideAddToCardBtn || (
                <svg
                    xmlns="http://www.w3.org/2000/svg"
                    fill="currentColor"
                    className="bi bi-x-lg delete-btn"
                    viewBox="0 0 16 16"
                    data-to-remove={true}
                    onClick={handleClick}
                >
                    <path
                        data-to-remove={true}
                        d="M2.146 2.854a.5.5 0 1 1 .708-.708L8 7.293l5.146-5.147a.5.5 0 0 1 .708.708L8.707 8l5.147 5.146a.5.5 0 0 1-.708.708L8 8.707l-5.146 5.147a.5.5 0 0 1-.708-.708L7.293 8 2.146 2.854Z"
                    />
                </svg>
            )}
            <div>
                <img className="product-img" src={img} alt={title} />
                <svg
                    xmlns="http://www.w3.org/2000/svg"
                    fill="currentColor"
                    className={props.svgClassName}
                    viewBox="0 0 16 16"
                    onClick={favourites}
                >
                    <path d="M2.866 14.85c-.078.444.36.791.746.593l4.39-2.256 4.389 2.256c.386.198.824-.149.746-.592l-.83-4.73 3.522-3.356c.33-.314.16-.888-.282-.95l-4.898-.696L8.465.792a.513.513 0 0 0-.927 0L5.354 5.12l-4.898.696c-.441.062-.612.636-.283.95l3.523 3.356-.83 4.73zm4.905-2.767-3.686 1.894.694-3.957a.565.565 0 0 0-.163-.505L1.71 6.745l4.052-.576a.525.525 0 0 0 .393-.288L8 2.223l1.847 3.658a.525.525 0 0 0 .393.288l4.052.575-2.906 2.77a.565.565 0 0 0-.163.506l.694 3.957-3.686-1.894a.503.503 0 0 0-.461 0z" />
                </svg>
            </div>
            <p className="product-title">{title}</p>
            <p className="product-price">{price}</p>
            {props.notHideAddToCardBtn && (
                <Button
                    className="product-btn"
                    text="Add to Cart"
                    onClick={handleClick}
                />
            )}
        </div>
    );
}

Product.propTypes = {
    data: PropTypes.object,
    addToCart: PropTypes.func,
    favourites: PropTypes.func,
};
export default Product;
