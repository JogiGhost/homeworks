import { Component } from "react";

class Button extends Component {
    render() {
        const { backgroundColor, text, onClick, className } = this.props;
        return (
            <button
                className={className}
                style={{ backgroundColor: backgroundColor }}
                onClick={onClick}
            >
                {text}
            </button>
        );
    }
}

export default Button;
