import { Component } from "react";
import Button from "./Button";
import "../scss/style.scss";
class Modal extends Component {
    render() {
        const { header, closeButton, text, actions, closeFunction } =
            this.props.data;
        return (
            <div className="modal-bg close-function" onClick={closeFunction}>
                <div className="modal">
                    <div className="modal-header">
                        <h1>{header}</h1>
                        {closeButton && (
                            <Button
                                className="close-function"
                                text="X"
                                onClick={closeFunction}
                            />
                        )}
                    </div>
                    <p>{text}</p>
                    <div className="modal-footer">{actions}</div>
                </div>
            </div>
        );
    }
}

export default Modal;
