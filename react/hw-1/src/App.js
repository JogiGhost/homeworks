import Button from "./components/Button";
import { Component } from "react";
import Modal from "./components/Modal";

class App extends Component {
    constructor() {
        super();
        this.state = { modal: "" };
    }
    handleClick = (e) => {
        if (e.target.textContent === "Open first modal") {
            this.setState({
                modal: {
                    header: "First modal",
                    closeButton: true,
                    text: `Lorem ipsum, dolor sit amet consectetur adipisicing elit. Perferendis
                    aliquam quo numquam. Rerum ducimus animi porro ipsa quas laborum
                    sapiente vitae expedita assumenda est illum repellat adipisci, cum ipsam
                    voluptatem!`,
                    actions: (
                        <>
                            <Button
                                backgroundColor="orange"
                                text="First Button"
                            />
                            <Button
                                backgroundColor="purple"
                                text="Second Button"
                            />
                        </>
                    ),
                    closeFunction: this.handleCloseClick,
                },
            });
        } else if (e.target.textContent === "Open second modal") {
            this.setState({
                modal: {
                    header: "Second modal",
                    closeButton: false,
                    text: `This text I wrote to fill the void in my heart and in the modal`,
                    actions: (
                        <>
                            <Button
                                backgroundColor="aqua"
                                text="First Button"
                            />
                            <Button
                                backgroundColor="yellowgreen"
                                text="Second Button"
                            />
                        </>
                    ),
                    closeFunction: this.handleCloseClick,
                },
            });
        }
    };
    handleCloseClick = (e) => {
        e.stopPropagation();
        if (e.target.className.includes("close-function")) {
            this.setState({ modal: "" });
        }
    };
    render() {
        return (
            <>
                <h1>Нету фотошопа, так что дизайн модалок прикрутил сам</h1>
                <Button
                    backgroundColor="blue"
                    text="Open first modal"
                    onClick={this.handleClick}
                />
                <Button
                    backgroundColor="yellow"
                    text="Open second modal"
                    onClick={this.handleClick}
                />
                {this.state.modal && <Modal data={this.state.modal} />}
            </>
        );
    }
}

export default App;
