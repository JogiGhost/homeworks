import { useSelector } from "react-redux";
import Form from "../../components/Form/Form";
import Main from "../../components/Main/Main";
import "./style.scss";

export default function Basket() {
    const addToCartList = useSelector((state) => state.addToCartList);
    return (
        <>
            <h2 className="basket-title">Basket</h2>
            <Form />
            <Main
                products={addToCartList}
                notHideAddToCardBtn={false}
                modalTitle="Remove from basket?"
            />
        </>
    );
}
