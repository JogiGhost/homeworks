import renderer from "react-test-renderer";
import Error from "./Errors";

it("renders correctly", () => {
    const tree = renderer.create(<Error></Error>).toJSON();
    expect(tree).toMatchSnapshot();
});
