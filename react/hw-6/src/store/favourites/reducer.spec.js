import { describe, expect } from "@jest/globals";
import favouritesReducer from "./reducer";

describe("favourites reducer", () => {
    it("should return the initial state", () => {
        expect(favouritesReducer(undefined, {})).toEqual([]);
    });
    it("should handle GET_FAVOURITES_LIST", () => {
        expect(
            favouritesReducer([], {
                type: "GET_FAVOURITES_LIST",
                payload: [
                    { title: "two" },
                    { title: "three" },
                    { title: "one" },
                ],
            })
        ).toEqual([{ title: "two" }, { title: "three" }, { title: "one" }]);
    });
    it("should handle SET_FAVOURITES_LIST without matching", () => {
        expect(
            favouritesReducer(
                [{ title: "two" }, { title: "three" }, { title: "one" }],
                {
                    type: "SET_FAVOURITES_LIST",
                    payload: { title: "four" },
                }
            )
        ).toEqual([
            { title: "two" },
            { title: "three" },
            { title: "one" },
            { title: "four" },
        ]);
    });
    it("should handle SET_FAVOURITES_LIST with matching", () => {
        expect(
            favouritesReducer(
                [{ title: "two" }, { title: "three" }, { title: "one" }],
                {
                    type: "SET_FAVOURITES_LIST",
                    payload: { title: "two" },
                }
            )
        ).toEqual([{ title: "three" }, { title: "one" }]);
    });
});
