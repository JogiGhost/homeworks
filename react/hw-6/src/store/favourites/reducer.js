const favouritesReducer = (state = [], action) => {
    switch (action.type) {
        case "GET_FAVOURITES_LIST":
            return action.payload;
        case "SET_FAVOURITES_LIST":
            if (state.some((e) => e.title === action.payload.title)) {
                const newFavArr = state.filter(
                    (elem) => elem.title !== action.payload.title
                );
                return newFavArr;
            }
            return [...state, action.payload];
        default:
            return state;
    }
};
export default favouritesReducer;
