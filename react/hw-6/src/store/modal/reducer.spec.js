import { describe, expect } from "@jest/globals";
import modalReducer from "./reducer";

describe("modal reducer", () => {
    it("should return the initial state", () => {
        expect(modalReducer(undefined, {})).toBe(false);
    });
    it("should handle TOGGLE_MODAL", () => {
        expect(
            modalReducer(false, {
                type: "TOGGLE_MODAL",
                payload: true,
            })
        ).toBe(true);
    });
});
