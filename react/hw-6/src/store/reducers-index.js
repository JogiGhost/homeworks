import modalReducer from "./modal/reducer";
import productsReducer from "./products/reducer";
import favouritesReducer from "./favourites/reducer";
import addToCartReducer from "./addToCart/reducer";
import { combineReducers } from "redux";

const mainReducer = combineReducers({
    isOpenModal: modalReducer,
    products: productsReducer,
    favouritesList: favouritesReducer,
    addToCartList: addToCartReducer,
});

export default mainReducer;
