import { describe, expect } from "@jest/globals";
import addToCartReducer from "./reducer";

describe("add to cart reducer", () => {
    it("should return the initial state", () => {
        expect(addToCartReducer(undefined, {})).toEqual([]);
    });
    it("should handle GET_CART_LIST", () => {
        expect(
            addToCartReducer([], {
                type: "GET_CART_LIST",
                payload: ["one"],
            })
        ).toEqual(["one"]);
    });
    it("should handle SET_CART_LIST", () => {
        expect(
            addToCartReducer([{ name: "two" }, { name: "three" }], {
                type: "SET_CART_LIST",
                payload: { name: "one" },
            })
        ).toEqual([{ name: "two" }, { name: "three" }, { name: "one" }]);
    });
    it("should handle DELETE_CART", () => {
        expect(
            addToCartReducer(
                [{ name: "two" }, { name: "three" }, { name: "one" }],
                {
                    type: "DELETE_CART",
                    payload: 1,
                }
            )
        ).toEqual([{ name: "two" }, { name: "one" }]);
    });
    it("should handle CLEAR_CART_LIST", () => {
        expect(
            addToCartReducer(
                [{ name: "two" }, { name: "three" }, { name: "one" }],
                { type: "CLEAR_CART_LIST" }
            )
        ).toEqual([]);
    });
});
