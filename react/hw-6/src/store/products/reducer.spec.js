import { describe, expect } from "@jest/globals";
import productsReducer from "./reducer";

describe("modal reducer", () => {
    it("should return the initial state", () => {
        expect(productsReducer(undefined, {})).toEqual([]);
    });
    it("should handle GET_PRODUCTS", () => {
        expect(
            productsReducer([], {
                type: "GET_PRODUCTS",
                payload: [{ name: "one" }, { name: "two" }, { name: "three" }],
            })
        ).toEqual([{ name: "one" }, { name: "two" }, { name: "three" }]);
    });
});
