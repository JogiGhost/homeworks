export const getProducts = () => async (dispatch) => {
    const response = await fetch("/products.json");
    const data = await response.json();
    dispatch({ type: "GET_PRODUCTS", payload: data });
};
