import Button from "../Button/Button.jsx";
import PropTypes from "prop-types";
import "./style.scss";

function Modal(props) {
    const { header, closeButton, text, actions, closeFunction } = props.data;
    return (
        <div
            className="modal-bg close-function"
            onClick={closeFunction}
            role="dialog"
        >
            <div className="modal">
                <div className="modal-header">
                    <h1>{header}</h1>
                    {closeButton && (
                        <Button
                            className="close-function close-btn"
                            text="X"
                            onClick={closeFunction}
                        />
                    )}
                </div>
                <p>{text}</p>
                <div className="modal-footer">{actions && actions}</div>
            </div>
        </div>
    );
}
Modal.propTypes = {
    data: PropTypes.object,
};
export default Modal;
