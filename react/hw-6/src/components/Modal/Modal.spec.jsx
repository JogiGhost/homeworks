import { describe, expect } from "@jest/globals";
import { render, screen, fireEvent } from "@testing-library/react";
import Button from "../Button/Button";
import Modal from "./Modal";
import renderer from "react-test-renderer";

describe("<Modal/> component test", () => {
    it("should render a modal", () => {
        const data = {
            header: "",
            closeButton: "",
            text: "",
            actions: "",
            closeFunction: () => {},
        };
        render(<Modal data={data} />);
        screen.getByRole("dialog");
        const tree = renderer.create(<Modal data={data} />).toJSON();
        expect(tree).toMatchSnapshot();
    });
    it("should render a modal with props", () => {
        const handleClick = jest.fn();
        const data = {
            header: "Modal",
            closeButton: true,
            text: "Some testing text",
            actions: (
                <>
                    <Button
                        backgroundColor="green"
                        text="Confirm"
                        className="close-function"
                    />
                    <Button
                        backgroundColor="red"
                        text="Cancel"
                        className="close-function"
                    />
                </>
            ),
            closeFunction: handleClick,
        };
        render(<Modal data={data} />);
        const closeBtn = screen.getByText("X");
        fireEvent.click(closeBtn);
        screen.getByText("Modal");
        screen.getByText("Some testing text");
        screen.getByText("Confirm");
        screen.getByText("Cancel");
        expect(handleClick).toHaveBeenCalledTimes(2);
    });
});
