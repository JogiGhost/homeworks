import * as Yup from "yup";
import { Formik, Form, Field } from "formik";
import { useSelector, useDispatch } from "react-redux";
import "./style.scss";
import { clearAddToCartList } from "../../store/addToCart/actions";

function BasketForm() {
    const validation = Yup.object().shape({
        firstName: Yup.string().required("Required"),
        lastName: Yup.string().required("Required"),
        age: Yup.number()
            .required("Required")
            .positive("Must be positive")
            .integer(),
        address: Yup.string().required("Required"),
        phone: Yup.number()
            .required("Required")
            .positive("Must be positive")
            .integer(),
    });
    const basketProducts = useSelector((state) => state.addToCartList);
    const dispatch = useDispatch();
    return (
        <>
            {basketProducts.length > 0 && (
                <div>
                    <Formik
                        initialValues={{
                            firstName: "",
                            lastName: "",
                            age: "",
                            address: "",
                            phone: "",
                        }}
                        validationSchema={validation}
                        onSubmit={(values) => {
                            console.log("FORM VALUES:", values);
                            console.log("PRODUCTS:", basketProducts);
                            dispatch(clearAddToCartList());
                        }}
                    >
                        {({ errors, touched }) => (
                            <Form className="form">
                                <div className="form-item">
                                    <label htmlFor="firstName">
                                        First Name
                                    </label>
                                    <Field
                                        name="firstName"
                                        id="firstName"
                                        placeholder="First Name"
                                    />
                                    {errors.firstName && touched.firstName ? (
                                        <div className="form-error">
                                            {errors.firstName}
                                        </div>
                                    ) : null}
                                </div>
                                <div className="form-item">
                                    <label htmlFor="lastName">Last Name</label>
                                    <Field
                                        name="lastName"
                                        id="lastName"
                                        placeholder="Last Name"
                                    />
                                    {errors.lastName && touched.lastName ? (
                                        <div className="form-error">
                                            {errors.lastName}
                                        </div>
                                    ) : null}
                                </div>
                                <div className="form-item">
                                    <label htmlFor="age">Age</label>
                                    <Field
                                        name="age"
                                        type="number"
                                        id="age"
                                        placeholder="Age"
                                    />
                                    {errors.age && touched.age ? (
                                        <div className="form-error">
                                            {errors.age}
                                        </div>
                                    ) : null}
                                </div>
                                <div className="form-item">
                                    <label htmlFor="address">Address</label>
                                    <Field
                                        name="address"
                                        type="text"
                                        id="address"
                                        placeholder="Address"
                                    />
                                    {errors.address && touched.address ? (
                                        <div className="form-error">
                                            {errors.address}
                                        </div>
                                    ) : null}
                                </div>
                                <div className="form-item">
                                    <label htmlFor="phone">Phone</label>
                                    <Field
                                        name="phone"
                                        type="number"
                                        id="phone"
                                        placeholder="Phone"
                                    />
                                    {errors.phone && touched.phone ? (
                                        <div className="form-error">
                                            {errors.phone}
                                        </div>
                                    ) : null}
                                </div>
                                <button type="submit">Checkout</button>
                            </Form>
                        )}
                    </Formik>
                </div>
            )}
        </>
    );
}

export default BasketForm;
