import { describe, expect } from "@jest/globals";
import { render, screen, fireEvent } from "@testing-library/react";
import renderer from "react-test-renderer";
import Button from "./Button";

describe("<Button/> component tests", () => {
    it("should render component", () => {
        render(<Button />);
        screen.getByRole("button");
        const tree = renderer.create(<Button />).toJSON();
        expect(tree).toMatchSnapshot();
    });
    it("render a button with props", () => {
        const handleClick = jest.fn();
        render(
            <Button
                backgroundColor="green"
                text="Click me!"
                onClick={handleClick}
                className="button"
            />
        );
        const btn = screen.getByRole("button");
        fireEvent.click(btn);
        expect(handleClick).toHaveBeenCalledTimes(1);
        expect(btn.style.backgroundColor).toBe("green");
        expect(btn.textContent).toBe("Click me!");
        expect(btn.className).toBe("button");
    });
});
