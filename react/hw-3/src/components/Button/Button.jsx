import PropTypes from "prop-types";

function Button(props) {
    const { backgroundColor, text, onClick, className } = props;
    return (
        <button
            className={className}
            style={{ backgroundColor: backgroundColor }}
            onClick={onClick}
        >
            {text}
        </button>
    );
}
Button.defaultProps = {
    backgroundColor: "",
    className: "",
};
Button.propTypes = {
    text: PropTypes.string,
    onClick: PropTypes.func,
    backgroundColor: PropTypes.string,
    className: PropTypes.string,
};
export default Button;
