import { useState } from "react";
import Button from "../Button/Button";
import Modal from "../Modal/Modal";
import Product from "../Product/Product";
import { PropTypes } from "prop-types";
import "./style.scss";

function Main(props) {
    const [modal, setModal] = useState(false);
    const [data, setData] = useState("");
    const [index, setIndex] = useState(null);
    const { products, notHideAddToCardBtn, modalTitle } = props;

    const handleClick = (info, index = null) => {
        document.body.style.overflow = "hidden";
        setModal(true);
        setIndex(index);
        setData(info);
    };
    const handleCloseClick = (e) => {
        e.stopPropagation();
        if (e.target.className.includes("close-function")) {
            document.body.style.overflow = "";
            setModal(false);
        }
    };
    const handleAddToFavClick = (data) => {
        props.handleAddToFavClick(data);
    };
    const handleConfirmClick = () => {
        props.handleDeleteCard && props.handleDeleteCard(index);
        props.handleAddToCartClick && props.handleAddToCartClick(data);
    };
    const modalConfig = {
        header: modalTitle,
        actions: (
            <>
                <Button
                    backgroundColor="green"
                    text="Confirm"
                    className="close-function"
                    onClick={handleConfirmClick}
                />
                <Button
                    backgroundColor="red"
                    text="Cancel"
                    className="close-function"
                />
            </>
        ),
        closeFunction: handleCloseClick,
    };
    return (
        <main className="main">
            {products &&
                products.map((product, index) => {
                    let svgClassName;
                    if (
                        props.favoritesList &&
                        props.favoritesList.some(
                            (elem) => product.title === elem.title
                        )
                    ) {
                        svgClassName = "bi bi-star bi-star-cart favourite";
                    } else {
                        svgClassName = "bi bi-star bi-star-cart";
                    }
                    return (
                        <Product
                            key={notHideAddToCardBtn ? product.art : index}
                            index={!notHideAddToCardBtn && index}
                            data={product}
                            handleClick={handleClick}
                            handleCloseClick={handleCloseClick}
                            svgClassName={svgClassName}
                            handleAddToCartClick={handleConfirmClick}
                            favourites={handleAddToFavClick}
                            notHideAddToCardBtn={notHideAddToCardBtn}
                        />
                    );
                })}
            {modal && <Modal data={modalConfig} />}
        </main>
    );
}
Main.defaultProps = {
    modalTitle: null,
    notHideAddToCardBtn: true,
};
Main.propTypes = {
    products: PropTypes.oneOfType([PropTypes.string, PropTypes.array]),
    handleAddToFavClick: PropTypes.oneOfType([
        PropTypes.string,
        PropTypes.func,
    ]),
    handleAddToCartClick: PropTypes.oneOfType([
        PropTypes.string,
        PropTypes.func,
    ]),
};
export default Main;
