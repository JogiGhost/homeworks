import { Route, Routes } from "react-router-dom";
import Main from "../components/Main/Main";
import Basket from "../Pages/Basket/Basket";
import Error from "../Pages/Errors/Errors";
import Favourites from "../Pages/Favourites/Favourites";

export default function Router(props) {
    const handleAddToFavClick = (data) => {
        props.data.handleAddToFavClick(data);
    };
    const handleAddToCartClick = (data) => {
        props.data.handleAddToCartClick(data);
    };
    const handleDeleteCard = (index) => {
        props.data.handleDeleteCard(index);
    };
    const { products, favoritesList, addToCartList } = props.data;
    return (
        <Routes>
            <Route path="/">
                <Route
                    index
                    element={
                        <Main
                            products={products}
                            modalTitle="Add to Cart ?"
                            favoritesList={favoritesList}
                            handleAddToFavClick={handleAddToFavClick}
                            handleAddToCartClick={handleAddToCartClick}
                        />
                    }
                ></Route>
                <Route
                    path="basket"
                    element={
                        <Basket
                            addToCartList={addToCartList}
                            favoritesList={favoritesList}
                            handleAddToFavClick={handleAddToFavClick}
                            handleDeleteCard={handleDeleteCard}
                        />
                    }
                ></Route>
                <Route
                    path="favourites"
                    element={
                        <Favourites
                            favoritesList={favoritesList}
                            handleAddToCartClick={handleAddToCartClick}
                            handleAddToFavClick={handleAddToFavClick}
                        />
                    }
                ></Route>
            </Route>
            <Route path="*" element={<Error />}></Route>
        </Routes>
    );
}
