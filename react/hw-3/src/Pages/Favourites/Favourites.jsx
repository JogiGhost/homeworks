import Main from "../../components/Main/Main";
import "./style.scss";

export default function Favourites(props) {
    const handleAddToFavClick = (data) => {
        props.handleAddToFavClick(data);
    };
    const handleAddToCartClick = (data) => {
        props.handleAddToCartClick(data);
    };
    const { favoritesList } = props;
    return (
        <>
            <h2 className="favourites-title">Favourites</h2>
            <Main
                modalTitle={"Add to Cart ?"}
                products={favoritesList}
                favoritesList={favoritesList}
                handleAddToCartClick={handleAddToCartClick}
                handleAddToFavClick={handleAddToFavClick}
            />
        </>
    );
}
