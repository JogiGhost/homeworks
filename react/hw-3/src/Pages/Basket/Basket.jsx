import Main from "../../components/Main/Main";
import "./style.scss";

export default function Basket(props) {
    const handleDeleteCard = (index) => {
        props.handleDeleteCard(index);
    };
    const handleAddToFavClick = (data) => {
        props.handleAddToFavClick(data);
    };
    const { addToCartList, favoritesList } = props;
    return (
        <>
            <h2 className="basket-title">Basket</h2>
            <Main
                products={addToCartList}
                favoritesList={favoritesList}
                handleAddToFavClick={handleAddToFavClick}
                handleDeleteCard={handleDeleteCard}
                notHideAddToCardBtn={false}
                modalTitle="Remove from basket?"
            />
        </>
    );
}
