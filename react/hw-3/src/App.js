import { useState, useEffect, useRef } from "react";
import Header from "./components/Header/Header";
import Router from "./Router/Router";

function App() {
    const [products, setProducts] = useState("");
    const [favoritesList, setFavoritesList] = useState([]);
    const [addToCartList, setAddToCartList] = useState([]);
    const isNotInitialMount = useRef(false);

    useEffect(() => {
        fetch("/products.json")
            .then((response) => response.json())
            .then((data) => {
                setProducts(data);
            });
        const counters = localStorage.getItem("counters");
        const { favoritesList, addToCartList } = JSON.parse(counters)
            ? JSON.parse(counters)
            : { favoritesList: [], addToCartList: [] };
        setFavoritesList(favoritesList);
        setAddToCartList(addToCartList);
    }, []);

    useEffect(() => {
        if (isNotInitialMount.current) {
            localStorage.setItem(
                "counters",
                JSON.stringify({
                    favoritesList: favoritesList,
                    addToCartList: addToCartList,
                })
            );
        }
        isNotInitialMount.current = true;
    }, [favoritesList, addToCartList]);

    const handleAddToCartClick = (data) => {
        document.body.style.overflow = "";
        setAddToCartList([...addToCartList, data]);
    };

    const handleDeleteCard = (index) => {
        setAddToCartList(addToCartList.filter((_, i) => i !== index));
    };
    const handleAddToFavClick = (data) => {
        if (favoritesList.some((e) => e.title === data.title)) {
            const newFavArr = favoritesList.filter(
                (elem) => elem.title !== data.title
            );
            setFavoritesList(newFavArr);
            return;
        }
        setFavoritesList([...favoritesList, data]);
    };

    return (
        <>
            <Header
                addToCartCounter={addToCartList && addToCartList.length}
                favoritesCounter={favoritesList && favoritesList.length}
            />
            <Router
                data={{
                    products: products,
                    favoritesList: favoritesList,
                    addToCartList: addToCartList,
                    handleAddToCartClick: handleAddToCartClick,
                    handleAddToFavClick: handleAddToFavClick,
                    handleDeleteCard: handleDeleteCard,
                }}
            />
        </>
    );
}

export default App;
