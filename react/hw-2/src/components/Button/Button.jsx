import { Component } from "react";
import PropTypes from "prop-types";

class Button extends Component {
    render() {
        const { backgroundColor, text, onClick, className } = this.props;
        return (
            <button
                className={className}
                style={{ backgroundColor: backgroundColor }}
                onClick={onClick}
                data-button-type={this.props.dataTags}
            >
                {text}
            </button>
        );
    }
}
Button.defaultProps = {
    backgroundColor: "",
    className: "",
};
Button.propTypes = {
    text: PropTypes.string,
    onClick: PropTypes.func,
    backgroundColor: PropTypes.string,
    className: PropTypes.string,
};
export default Button;
