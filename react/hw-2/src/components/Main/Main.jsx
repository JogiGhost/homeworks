import { Component } from "react";
import Button from "../Button/Button";
import Modal from "../Modal/Modal";
import Product from "../Product/Product";
import { PropTypes } from "prop-types";
import "./style.scss";

class Main extends Component {
    constructor() {
        super();
        this.state = {
            modal: false,
        };
    }
    handleClick = (e, data) => {
        if (e.target.dataset.buttonType === "add-to-card") {
            document.body.style.overflow = "hidden";
            this.cardData = data;
            this.setState({
                modal: true,
            });
        }
    };
    handleCloseClick = (e) => {
        e.stopPropagation();
        if (e.target.className.includes("close-function")) {
            document.body.style.overflow = "";
            this.setState({ modal: false });
        }
    };
    handleAddToFavClick = (data) => {
        this.props.handleAddToFavClick(data);
    };
    handleAddToCartClick = () => {
        this.props.handleAddToCartClick(this.cardData);
    };
    render() {
        const { products } = this.props;
        const modalConfig = {
            header: "Add to Cart ?",
            actions: (
                <>
                    <Button
                        backgroundColor="green"
                        text="Confirm"
                        className="close-function"
                        onClick={this.handleAddToCartClick}
                    />
                    <Button
                        backgroundColor="red"
                        text="Cancel"
                        className="close-function"
                    />
                </>
            ),
            closeFunction: this.handleCloseClick,
        };
        return (
            <main className="main">
                {products &&
                    products.map((product) => {
                        let svgClassName;
                        if (
                            this.props.favoritesList &&
                            this.props.favoritesList.some(
                                (elem) => product.title === elem.title
                            )
                        ) {
                            svgClassName = "bi bi-star bi-star-cart favourite";
                        } else {
                            svgClassName = "bi bi-star bi-star-cart";
                        }
                        return (
                            <Product
                                key={product.art}
                                data={product}
                                svgClassName={svgClassName}
                                addToCart={this.handleClick}
                                favourites={this.handleAddToFavClick}
                            />
                        );
                    })}
                {this.state.modal && <Modal data={modalConfig} />}
            </main>
        );
    }
}
Main.propTypes = {
    products: PropTypes.oneOfType([PropTypes.string, PropTypes.array]),
    handleAddToFavClick: PropTypes.oneOfType([
        PropTypes.string,
        PropTypes.func,
    ]),
    handleAddToCartClick: PropTypes.oneOfType([
        PropTypes.string,
        PropTypes.func,
    ]),
};
export default Main;
