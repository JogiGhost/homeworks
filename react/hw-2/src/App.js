import { Component } from "react";
import Header from "./components/Header/Header";
import Main from "./components/Main/Main";

class App extends Component {
    constructor() {
        super();
        this.state = {
            products: "",
        };
    }
    componentDidMount = () => {
        fetch("/products.json")
            .then((response) => response.json())
            .then((data) => {
                this.setState({ products: data });
            });
        const counters = localStorage.getItem("counters");
        const { favoritesList, addToCartList } = JSON.parse(counters)
            ? JSON.parse(counters)
            : { favoritesList: [], addToCartList: [] };
        this.setState({
            favoritesList: favoritesList,
            addToCartList: addToCartList,
        });
    };
    componentDidUpdate = () => {
        localStorage.setItem(
            "counters",
            JSON.stringify({
                favoritesList: this.state.favoritesList,
                addToCartList: this.state.addToCartList,
            })
        );
    };
    handleAddToCartClick = (data) => {
        document.body.style.overflow = "";
        this.setState({
            addToCartList: [...this.state.addToCartList, data],
        });
    };
    handleAddToFavClick = (data) => {
        if (this.state.favoritesList.some((e) => e.title === data.title)) {
            return;
        }
        this.setState({
            favoritesList: [...this.state.favoritesList, data],
        });
    };
    render() {
        return (
            <>
                <Header
                    addToCartCounter={
                        this.state.addToCartList &&
                        this.state.addToCartList.length
                    }
                    favoritesCounter={
                        this.state.favoritesList &&
                        this.state.favoritesList.length
                    }
                />
                <Main
                    products={this.state.products}
                    favoritesList={this.state.favoritesList}
                    handleAddToFavClick={this.handleAddToFavClick}
                    handleAddToCartClick={this.handleAddToCartClick}
                />
            </>
        );
    }
}

export default App;
